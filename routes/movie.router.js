const movieController = require('../controller/movie.controller');
const pino = require('pino')();

module.exports = router => {
    router
        .get('/api/movies', listMovies)
        .get('/api/movies/:id', findMovie)
        .post('/api/movies', findAndReplacePlot);
};

const findMovie = ctx => {
    try {
        const yearParam = ctx.get('year-param');
        if (isNaN(yearParam)) {
            ctx.body = {
                ok: false,
                data: 'invalid header parameter'
            };
            ctx.status = 400;
            throw ctx.body;
        }
        return movieController.searchMovie(ctx);
    } catch (error) {
        pino.error(error);
    }
};

const listMovies = ctx => {
    try {
        const pageParam = ctx.get('page-param');
        if (isNaN(pageParam) || pageParam <= 0) {
            ctx.body = {
                ok: false,
                data: 'invalid header parameter'
            };
            ctx.status = 400;
            throw ctx.body;
        }
        return movieController.listMovie(ctx);
    } catch (error) {
        pino.error(error);
    }

};

const findAndReplacePlot = async ctx => {
    try {
        await movieController.findAndReplace(ctx);

    } catch (error) {
        pino.error(error);
    }

};