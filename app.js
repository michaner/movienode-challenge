require('dotenv').config();
const Koa = require('koa');
const Router = require('koa-router');
const respond = require('koa-respond');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser')
const {
    dbConnection
} = require('./database/config');

const app = new Koa();
const router = new Router();

dbConnection();
app.use(respond());
app.use(cors());
app.use(bodyParser());

require('./routes/movie.router')(router);

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT || 3000, () =>{
    console.log('koa running');
});