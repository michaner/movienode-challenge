const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const pino = require('pino')();
const dbConnection = async () => {
    try {
        mongoose.connect(process.env.DB_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        console.log('Connected to mongodb')
    } catch (error) {
        pino.error(error);                
    }
}

module.exports = {
    dbConnection
}