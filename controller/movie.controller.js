const movieModel = require('../model/movie.model');
const fetch = require('node-fetch');
const util = require('../helpers/util.helper');
const pino = require('pino')();

module.exports = class Movie {
    static async searchMovie(ctx) {
        const response = await findMovieApi(ctx);
        if (!response.ok) {
            return response;
        }
        await validateAndSaveMovie(response.data, ctx)
        return ctx.body;
    }

    static listMovie(ctx) {
        return movieModel.find().exec()
            .then((res) => {
                const pageNumber = ctx.get('page-param');
                const startIndex = (pageNumber - 1) * 5;
                const pageSlice = res.slice(startIndex, pageNumber * 5);
                if (pageSlice.length > 0) {
                    ctx.body = {
                        ok: true,
                        data: pageSlice
                    };
                } else {
                    ctx.status = 404;
                    ctx.body = {
                        ok: false,
                        data: 'Not Found'
                    };
                    pino.error(ctx.body);
                }
            });
    }

    static async findAndReplace(ctx) {
        if (Object.keys(ctx.request.body).length === 0) {
            ctx.status = 400;
            ctx.body = {
                ok: false,
                data: 'Bad Request'
            };
            throw ctx.body;
        }
        const {
            movie,
            find,
            replace
        } = ctx.request.body;
        const isMovieExist = await findMovie(movie);
        const validate = validateMovie(movie, find, replace, ctx)
        return validate[isMovieExist]();
    }
}

const findMovie = (movie) => {
    return movieModel.exists({
        Title: movie
    });
}

const findMovieApi = async (ctx) => {
    const key = 'a66bfab9';
    const yearParam = ctx.get('year-param');
    let movieSaveModel = {};
    let url = `http://www.omdbapi.com/?apikey=${key}&t=${ctx.params.id}`;
    url = yearParam != '' ? `${url}&y=${yearParam}` : url;
    await fetch(url)
        .then(res => res.json())
        .then((json) => {
            if (json.Response === 'False') {
                ctx.status = 404;
                ctx.body = {
                    ok: false,
                    data: 'Not Found'
                };
            } else {
                const {
                    Title,
                    Year,
                    Released,
                    Genre,
                    Director,
                    Actors,
                    Plot,
                    Ratings
                } = json;

                movieSaveModel = {
                    Title,
                    Year,
                    Released,
                    Genre,
                    Director,
                    Actors,
                    Plot,
                    Ratings
                };
                ctx.body = {
                    ok: true,
                    data: movieSaveModel
                }
            }
        });
    return ctx.body;
}

const validateMovie = (movie, find, replace, ctx) => {
    return {
        true: async () => {
            await movieModel.findOne({
                Title: movie
            }).then(res => {
                const response = util.replaceAll(res.Plot, find, replace);
                ctx.body = {
                    ok: true,
                    data: response
                };
            }, error => {
                ctx.body = {
                    ok: false,
                    data: error
                };
                throw ctx.body;
            });
        },
        false: () => {
            ctx.status = 404;
            ctx.body = {
                ok: false,
                data: 'Not Found'
            };
            throw ctx.body;
        }
    };
}

const validateAndSaveMovie = async (movieSaveModel, ctx) => {
    const isMovieExist = await findMovie(movieSaveModel.Title);
    if (!isMovieExist) {
        await movieModel.create(movieSaveModel)
            .then((res) => {
                ctx.body = {
                    ok: true,
                    data: res
                };
            });
    } else {
        ctx.status = 406;
        ctx.body = {
            ok: false,
            data: 'already exist'
        };
        pino.error(ctx.body); 
    }
}