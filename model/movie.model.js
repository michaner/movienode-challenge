const {
    Schema,
    model
} = require('mongoose');

const movieSchema = new Schema({
    Title: {
        type: String
    },
    Year: {
        type: String
    },
    Released: {
        type: String
    },
    Genre: {
        type: String
    },
    Director: {
        type: String
    },
    Actors: {
        type: String
    },
    Plot: {
        type: String
    },
    Ratings: [{
        Source: String,
        Value: String
    }]
});

movieSchema.method('toJSON', function () {
    const {
        __v,
        _id,
        ...object
    } = this.toObject();
    return object;
});

module.exports = model('Movie', movieSchema);