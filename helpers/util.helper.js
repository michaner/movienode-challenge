const replaceAll = (value, search, replace) => {
    return value.split(search).join(replace);
}

module.exports = {replaceAll};